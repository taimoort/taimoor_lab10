;Add Function
(define add (lambda (m n s z)
(m s (n s z))))


;Subtract Function
(define sub (lambda (m n)
(m pred n)))


;And Function
(define and (lambda (m n a b)
(n (m a b)b)))


;Or Function
(define or (lambda (m n a b)
(n a (m a b))))


;Not Function
(define not (lambda (m a b)
(m b a)))


;LEQ Function
(define leq (lambda (m n)
(isZero (sub m n))))


;GEQ Function
(define geq (lambda (m n)
(or (isZero (sub (m n))) not (leq (m n)))))